package com.example.rockpaperscissorsgame

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import android.widget.Toast;

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View) {
        rockBtn.setOnClickListener(this)
        paperBtn.setOnClickListener(this)
        scissorsBtn.setOnClickListener(this)
        var choice = 0

        when(v.id){
            R.id.rockBtn -> {


                choice = 0
                checkResults(choice)
            }
            R.id.paperBtn -> {
                choice = 1
                checkResults(choice)
            }
            R.id.scissorsBtn -> {
                choice = 2
                checkResults(choice)
            }

        }

    }

    private fun checkResults(choice: Int) {
        val cpuChoice = (0..2).random()

        if (choice == 0 && cpuChoice == 0){ //user chose rock, cpu chose rock = tie
            displayMessage(0, "Rock");
        }else if(choice == 0 && cpuChoice == 1 )//user chose rock, cpu chose paper = user lost
            displayMessage(1, "Paper");
        else if(choice == 0 && cpuChoice == 2) //user chose rock, cpu chose scissors = user won
            displayMessage(2, "Scissors");
        else if(choice == 1 && cpuChoice == 0) //user chose paper, cpu chose rock = user won
            displayMessage(2, "Rock")
        else if(choice == 1 && cpuChoice == 1) //user chose paper, cpu chose paper = tie
            displayMessage(0, "Paper")
        else if(choice == 1 && cpuChoice == 2) //user chose paper, cpu chose scissors = cpu won
            displayMessage(1, "Scissors")
        else if(choice == 2 && cpuChoice == 0)//user chose scissors, cpu chose rock = cpu won
            displayMessage(1, "Rock")
        else if(choice == 2 && cpuChoice == 1) //user chose scissors, cpu chose paper = user won
            displayMessage(2, "Paper")
        else if(choice == 2 && cpuChoice == 2)//both scissors = tie
            displayMessage(0, "Scissors")
    }

    private fun displayMessage(i: Int, cpuChoice: String) {
        if(i == 0){
            Toast.makeText(getApplicationContext(), "CPU chose $cpuChoice. It's a Tie! Try Again!", Toast.LENGTH_SHORT).show();
        }else if(i == 1)
            Toast.makeText(getApplicationContext(), "CPU chose $cpuChoice. You Lose! Try Again!", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getApplicationContext(), "CPU chose $cpuChoice. You Won! Keep Playing!", Toast.LENGTH_SHORT).show();
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
9
    }
}
